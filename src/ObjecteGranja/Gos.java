package ObjecteGranja;

public class Gos {

	private String nom;
	private int edat;
	private char sexe;
	private int fills;
	private Ra�a ra�aGos;
	private GosEstat estatGos = GosEstat.VIU;
	private static int contador = 0;

	//
	// Constructor
	//

	// All valors\\
	// 1.A.3
	public Gos(String nom, int edat, char sexe, int fills) {
		this();
		this.nom = nom;
		this.edat = edat;
		this.sexe = sexe;
		this.fills = fills;
	}

	// Copia d'un altre gos\\
	// 1.A.4
	public Gos(Gos copia) {
		this();
		this.nom = copia.nom;
		this.edat = copia.edat;
		this.sexe = copia.sexe;
		this.fills = copia.fills;
	}

	// Por defecto (Sin parametros)\\
	// 1.A.4 & 1.A.5
	public Gos() {
		nom = "SenseNom";
		edat = 4;
		sexe = 'M';
		fills = 0;
		contador++;
	}

	public Gos(String nom, int edat, char sexe, int fills, Ra�a ra�aGos) {
		this(nom, edat, sexe, fills);
		this.ra�aGos = ra�aGos;
	}

	//
	// Metodes
	//

	// 1.A.1 Borda ladrar (guau guau)
	public void borda() {
		System.out.println("guau guau");
	}

	// 1.A.3 Visualitzar gos
	public void visualitzar() {
		System.out.println(this.toString());
	}

	// String toString
	@Override
	public String toString() {
		if (ra�aGos == null) {
			return "Nom: " + nom + "	Edat: " + edat + "	Sexe: " + sexe + "	Fills: " + fills;
		} else
			return "Nom: " + nom + "	Edat: " + edat + "	Sexe: " + sexe + "	Fills: " + fills + "	Ra�a: "
					+ ra�aGos;

	}

	// Clonar
	// 1.A.5
	public Gos clonar() {
		Gos GosClonat = new Gos();
		GosClonat.nom = nom;
		GosClonat.edat = edat;
		GosClonat.sexe = sexe;
		GosClonat.fills = fills;
		return GosClonat;
	}

	// Visualitzar quantitat Gossos
	// 1.A.6
	public static int quantitatGosos() {
		return contador;
	}
	
	public Gos aparellar(Gos g) {
		if (potAparellarse(g)) {

			// Generar fill
			Gos fill = new Gos();

			// Edat
			fill.setEdat(0);
			
			// Sexe
			if ((int) (Math.random() * 2) == 0)
				fill.setSexe('M');
			else
				fill.setSexe('F');

			// Nom
			if(fill.getSexe() == 'M')
				//Pare
				if(this.getSexe() == 'M')
					fill.setNom("Fill de " + this.getNom());
				else
					fill.setNom("Fill de " + g.getNom());
			else
				//Mare
				if(this.getSexe() == 'F')
					fill.setNom("Fill de " + this.getNom());
				else
					fill.setNom("Fill de "+ g.getNom());

			// Ra�a
			fill.setRa�aGos(calculaRa�aDominant(this.getRa�aGos(), g.getRa�aGos()));

			
			// Actualitzar pares
			this.setFills(this.getFills() + 1);
			g.setFills(g.getFills() + 1);
			
			return fill;
			
		} else
			return null;
	}
	
	// - Comprovar si pot aparellarse
	private boolean potAparellarse(Gos gos2) {

		// Edat (2 a 10 anys)
		if (this.getEdat() < 2 || this.getEdat() > 10)
			return false;

		// Mascle i Femella
		if (this.getSexe() == 'F' && gos2.getSexe() == 'F' || this.getSexe() == 'M' && gos2.getSexe() == 'M')
			return false;

		// Estan vius
		if (this.estatGos != GosEstat.VIU || gos2.getEstatGos() != GosEstat.VIU)
			return false;

		// 3 fills maxim
		if (this.getSexe() == 'F' && this.getFills() > 3 || gos2.getSexe() == 'F' && gos2.getFills() > 3)
			return false;

		// Tamanys compatibles
		if (this.getSexe() == 'M')
			// - Pasa com a mascle
			return tamanyCompatible(this, gos2);
		else
			// - Pasa com a femella
			return tamanyCompatible(gos2, this);

	}
	
	// VALIDAR TAMANY PARELLA
	private boolean tamanyCompatible(Gos Mascle, Gos Femella) {

		//Comprovar si t� ra�a (per poder comprovar la mida)
		if(Mascle.getRa�aGos() == null || Femella.getRa�aGos() == null)
			return false;
		
		// PETIT
		// - Sempre podra aparearse
		if (Mascle.ra�aGos.getMida() == GosMida.PETIT)
			return true;

		// MITJA
		// - Amb mitjans o grans
		if (Mascle.ra�aGos.getMida() == GosMida.MITJA) {
			switch (Femella.ra�aGos.getMida()) {
			case MITJA:
			case GRAN:
				return true;
			default:
				return false;
			}
		}

		// GRAN
		// - Nomes amb els grans
		if (Mascle.ra�aGos.getMida() == GosMida.GRAN && Femella.ra�aGos.getMida() == GosMida.GRAN)
			return true;
		else
			return false;
	}

	private Ra�a calculaRa�aDominant(Ra�a mascle, Ra�a femella) {

		// Comprova les ra�es i retorna la important
		if (mascle == null && femella == null)
			return null;
		else if (mascle == null)
			return femella;
		else if (femella == null)
			return mascle;

//		// MASCLE dominant
//		if (mascle.getDominant()) {
//
//			// FEMELLA tamb� dominant
//			if (femella.getDominant())
//				return femella;
//			else
//				return mascle;
//
//		} else
//			// MASCLE no dominant, sempre agafa femella
//			return femella;
		
		if(mascle.isDominant() && !femella.isDominant())
			return mascle;
		else
			return femella;

	}
	
	//
	// Getters and Setters
	//

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		if (ra�aGos == null && edat >= 10)
			estatGos = GosEstat.MORT;
		else if (ra�aGos != null && edat >= ra�aGos.getTempsVida())
			estatGos = GosEstat.MORT;

		this.edat = edat;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}

	public Ra�a getRa�aGos() {
		return ra�aGos;
	}

	public void setRa�aGos(Ra�a ra�aGos) {
		this.ra�aGos = ra�aGos;
	}

	public GosEstat getEstatGos() {
		return estatGos;
	}

	public void setEstatGos(GosEstat estatGos) {
		this.estatGos = estatGos;
	}

}
