package ObjecteGranja;

import java.util.Arrays;

public class Granja {

	private int numGosos;
	private int topGosos;
	private Gos[] gosos;

	// Constructor

	public Granja() {
		numGosos = 0;
		topGosos = 100;
		gosos = new Gos[topGosos];
	}

	Granja(int total) {
		if (total <= 1 || total > 100)
			topGosos = total;
		gosos = new Gos[topGosos];
		// numGosos = 0; //Inicializar granja
	}

	// Methodes

	public int afegir(Gos g) {
		if (numGosos < topGosos) {
			gosos[numGosos] = g;
			numGosos++;
			return numGosos;
		} else {
			return -1;
		}
	}

	public void visualitzar() {
		for (int i = 0; i < numGosos; i++) {
			System.out.println(gosos[i]);
		}
	}

	public void visualitzarVius() {
		for (int i = 0; i < numGosos; i++) {
			if (gosos[i].getEstatGos() == GosEstat.VIU) {
				System.out.println(gosos[i]);
			}
		}
	}

	public static Granja generarGranja(int top) {

		Granja gr = new Granja(top);

		// GENERAR GOSSOS

		// Generar noms
		String[] noms = { "Bethoven", "Jako", "Rex", "Bonie", "Max", "Jack", "Nina", "Chester", "Bobbie", "Zeus",
				"Blacky", "Day", "Melanie", "Wachimichu", "Luna", "Marlos", "Tommy", "Clifford", "Excalibur",
				"Franchesco", "Chelle" };

		// Generar races
		Ra�a[] races = { new Ra�a("Doberman", GosMida.MITJA, 12), new Ra�a("Chihuahua", GosMida.PETIT, 9),
				new Ra�a("Pastor alemany", GosMida.GRAN, 12), new Ra�a("Caniche", GosMida.PETIT, 8),
				new Ra�a("Labrador", GosMida.MITJA, 11), new Ra�a("Galgo", GosMida.MITJA, 12),
				new Ra�a("Mastin", GosMida.MITJA, 11), new Ra�a("Rottweiler", GosMida.MITJA, 10),
				new Ra�a("Corgi", GosMida.MITJA), new Ra�a("Bull terrier", GosMida.MITJA, 10), null // Sense ra�a
		};

		Gos newg;

		// Per cada espai disponible
		for (int i = 0; i < top; i++) {

			// Nova instancia de gos
			newg = new Gos();

			// Nombre
			newg.setNom(noms[(int) (Math.random() * noms.length)] + " " + (i + 1));
			;

			// Ra�a
			newg.setRa�aGos(races[(int) (Math.random() * races.length)]);

			// Edat
			int t;
			if (newg.getRa�aGos() == null)
				t = 10;
			else
				t = newg.getRa�aGos().getTempsVida() - 1;

			newg.setEdat((int) (Math.random() * t + 1));

			// Sexe
			if ((int) (Math.random() * 2) == 0)
				newg.setSexe('M');
			else
				newg.setSexe('F');

			// Dominant
			if (newg.getRa�aGos() != null)
				if ((int) (Math.random() * 2) == 0)
					newg.getRa�aGos().setDominant(true);
				else
					newg.getRa�aGos().setDominant(false);

			// Fills i Estat ja venen per defecte

			// AFEGIR GOS
			gr.afegir(newg);

		}

		return gr;
	}

	public Gos obtenirGos(int og) {
		if (gosos[og] == null) {
			return null;
		} else {
			return gosos[og];
		}
	}

	@Override
	public String toString() {
		return "Granja [numGosos=" + numGosos + ", topGosos=" + topGosos + ", gosos=" + Arrays.toString(gosos) + "]";
	}

	// Getters

	public int getNumGosos() {
		return numGosos;
	}

	public int getTopGosos() {
		return topGosos;
	}

	public Gos[] getGosos() {
		return gosos;
	}
}
