package ObjecteGranja;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

//		Scanner reader = new Scanner(System.in);

//		 // Crear Ra�a
//		 Ra�a ra�a1 = new Ra�a("Alejou", GosMida.GRAN, 10);
//		 Ra�a ra�a2 = new Ra�a("Josesito", GosMida.MITJA, 7);
//		 Ra�a ra�a3 = new Ra�a("Juanirito", GosMida.PETIT, 4);
//
//		 //Crear nuevo Gos
//		 Gos gos1 = new Gos("Max", 5, 'M', 0);
//		 Gos gos2 = new Gos();
//		 Gos gos3 = new Gos(gos1);
//		 Gos gos4 = gos2.clonar();
//
//		 //Imprimir Info Gos
//		 System.out.println(gos1);
//		 System.out.println(gos2);
//		 System.out.println(gos3);
//		 System.out.println(gos4);
//		
//		 //Borda (ladrar)
//		 gos1.borda();
//		 gos2.borda();
//		 gos3.borda();
//		 gos4.borda();
//		
//		 //Mostrar Dades
//		 System.out.println(ra�a1.toString());
//		 System.out.println(ra�a2.toString());
//		 System.out.println(ra�a3.toString());
//
//		 Gos gos11 = new Gos("Juan", 4, 'M', 0, ra�a3);
//		 Gos gos21 = new Gos("Yayao", 6, 'F', 0, ra�a1);
//		 Gos gos31 = new Gos("Pitingo", 2, 'M', 0, ra�a2);
//		 Gos gos41 = new Gos("Romero", 8, 'M', 0, ra�a2);
//		 Gos gos5 = new Gos("Albaca", 4, 'M', 0, ra�a1);
//		 Gos gos6 = new Gos("Yokese", 3, 'F', 0, ra�a3);
//		 Gos gos7 = new Gos("Juliet", 5, 'F', 0, ra�a2);
//		 Gos gos8 = new Gos("Arnorld", 1, 'M', 0, ra�a1);
//		 Gos gos9 = new Gos("Elisabeth", 8, 'F', 0, ra�a3);
//		 Gos gos10 = new Gos("Emilia", 2, 'F', 0, ra�a2);
//		
//		 Granja granja1 = new Granja();
//		 Granja granja2 = new Granja();
//		 Granja granja3 = new Granja();
//		
//		 granja1.afegir(gos11);
//		 granja1.afegir(gos21);
//		 granja1.afegir(gos31);
//		 granja1.afegir(gos41);
//		 granja1.afegir(gos5);
//		
//		 granja2.afegir(gos6);
//		 granja2.afegir(gos7);
//		 granja2.afegir(gos8);
//		 granja2.afegir(gos9);
//		 granja2.afegir(gos10);
//		
//		 int anys;
//		
//		 System.out.println("Di un n�mero de a�os del 1 al 20");
//		 anys = reader.nextInt();
//		
//		 if (anys < 1 || anys > 20) {
//		 System.out.println("Tiene que ser un n�mero entre 1 y 20");
//		 } else {
//		 for (int x = 0; x < anys; x++) {
//		 for (int i = 0; i < 5; i++) {
//		 Gos g = granja1.obtenirGos(i).aparellar(granja2.obtenirGos(i));
//		 if (g != null) {
//		 granja3.afegir(g);
//		 }
//		 }
//		 }
//		 }
//		 // Quantitat Gosos
//		 Gos.quantitatGosos();
//		 System.out.println(Gos.quantitatGosos());
//		
//		 System.out.println("Granja 1");
//		 granja1.visualitzar();
//		 System.out.println("Granja 2");
//		 granja2.visualitzar();
//		 System.out.println("Granja 3");
//		 granja3.visualitzar();
		
		creuarGranjes();
	}

	public static void creuarGranjes() {

		// Generar granjes
		Granja G1 = Granja.generarGranja(5);
		Granja G2 = Granja.generarGranja(5);
		Granja G3 = new Granja();

		Gos fill;

		// Per cada gos a la Granja 1
		for (int i = 0; i < G1.getNumGosos(); i++) {

			// Intentar aparellar
			fill = G1.obtenirGos(i).aparellar(G2.obtenirGos(i));

			// Si han pogut tenir un fill
			if (fill != null)
				// Afegir fill a la tercera granja
				G3.afegir(fill);
		}

		System.out.println("\n====== Granja 1 ======");
		G1.visualitzar();

		System.out.println("\n====== Granja 2 ======");
		G2.visualitzar();

		System.out.println("\n====== Granja 3 ======");
		G3.visualitzar();
	}

}
