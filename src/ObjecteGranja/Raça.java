package ObjecteGranja;

public class Ra�a {
	
	private String nomRa�a;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	//
	//Constructors
	//
	
	//Ra�a nom, mida, tempsVida
		//1.B.2 ii
	public Ra�a(String nom, GosMida mida, int tempsVida) {
		this(nom, mida);
		this.tempsVida = tempsVida;
	}
	
		//1.B.2 ii
	public Ra�a(String nom, GosMida mida) {
		this.nomRa�a = nom;
		this.mida = mida;
		tempsVida = 10;
		dominant = false;
	}
	
	//
	//Metodes
	//
	
	@Override
	public String toString() {
		return "NomRa�a: " + nomRa�a + "	GosMida: " + mida + "	Temps Vida: " + tempsVida + "	Dominant: " + dominant;
	}
	
	//
	//Getters & Setters
	//
	
	public String getNomRa�a() {
		return nomRa�a;
	}

	public void setNomRa�a(String nomRa�a) {
		this.nomRa�a = nomRa�a;
	}

	public GosMida getMida() {
		return mida;
	}

	public void setMida(GosMida mida) {
		this.mida = mida;
	}

	public int getTempsVida() {
		return tempsVida;
	}

	public void setTempsVida(int tempsVida) {
		this.tempsVida = tempsVida;
	}

	public boolean isDominant() {
		return dominant;
	}

	public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}
}
